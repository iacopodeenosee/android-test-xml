package idnlab.iacopodeenosee.test_xml;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;


public class XmlActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_xml);

        TextView tvw_fileXML = findViewById(R.id.tvw_fileXML);

        Intent intent = getIntent();

        String pkg = getPackageName();
        String filexml = intent.getStringExtra(pkg + ".strXML");
        if (filexml.equals("")) {
            Toast.makeText(getApplicationContext(), "STRING XML EMPTY", Toast.LENGTH_LONG);

            tvw_fileXML.setText("file XML: \n\n" + "ERROR");
            tvw_fileXML.setTextColor(Color.BLACK);

        } else {
            tvw_fileXML.setText("file XML: \n\n" + filexml);
            tvw_fileXML.setTextColor(Color.RED);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}

