package idnlab.iacopodeenosee.test_xml;


public class TouchMask {
    private String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return " comment= " + comment;
    }
}