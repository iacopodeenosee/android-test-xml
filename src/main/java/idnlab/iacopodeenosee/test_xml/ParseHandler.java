package idnlab.iacopodeenosee.test_xml;


import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ParseHandler {
    private List<TouchMask> listTouchMask = new ArrayList<TouchMask>();
    private TouchMask mTouchMask;
    private String text;

    public List<TouchMask> getTouchMasks() {
        return listTouchMask;
    }

    public List<TouchMask> parse(InputStream is) {
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();

            parser.setInput(is, null);

            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagname = parser.getName();
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagname.equalsIgnoreCase("touch_mask")) {
                            // create a new instance of mTouchMask
                            mTouchMask = new TouchMask();
                            Log.v("TEST30", "new toucmask obj");
                        }
                        break;

                    case XmlPullParser.TEXT:
                        text = parser.getText();
                        Log.v("TEST30", "new value text:" + text);
                        break;

                    case XmlPullParser.END_TAG:
                        if (tagname.equalsIgnoreCase("touch_mask")) {
                            // add mTouchMask object to list
                            Log.v("TEST30", "add toucmask obj");

                            listTouchMask.add(mTouchMask);
                        } else if (tagname.equalsIgnoreCase("comment")) {
                            Log.v("TEST30", "add COMMENT: " + text);

                            mTouchMask.setComment(text);
                        }
                        break;

                    default:
                        break;
                }
                eventType = parser.next();
            }

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return listTouchMask;
    }

    public TouchMask parseActiveTM(InputStream is, MatrixData md) {
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();

            //setup data matrix
            String ori = "";
            if (md.getOrientation() == 1) {
                ori = "portrait";
            } else {
                ori = "landscape";
            }
            int NumberT = md.getNumberT();
            String tmpType = md.getTypeInput();
            //setup matrix boolean
            boolean okInput, okOri, okNumT;
            okInput = okOri = okNumT = false;

            parser.setInput(is, null);

            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagname = parser.getName();
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagname.equalsIgnoreCase("input")) {
                            // get all parameters
                            String type = parser.getAttributeValue(null, "type");
                            Log.v("TEST31", "input attr: " + type);

                            if (type.equals(tmpType)) {
                                okInput = true;
                            }
                        } else if (tagname.equalsIgnoreCase(ori) && okInput == true) {
                            // get the right orietation tag
                            Log.v("TEST31", "orientation tag: " + ori);
                            okOri = true;
                        } else if (tagname.equalsIgnoreCase("touch_mask") && okOri == true) {
                            // get all parameters
                            String numT = parser.getAttributeValue(null, "touch");
                            Log.v("TEST31", "number touch attr: " + numT);

                            if (Integer.parseInt(numT) == NumberT) {
                                okNumT = true;
                                // create a new instance of mTouchMask
                                mTouchMask = new TouchMask();
                                Log.v("TEST31", "new toucmask obj");
                            }

                        }
                        break;

                    case XmlPullParser.TEXT:
                        text = parser.getText();
                        Log.v("TEST31", "new value text:" + text);
                        break;

                    case XmlPullParser.END_TAG:
                        if (tagname.equalsIgnoreCase("touch_mask") && okNumT) {
                            // return touchmask
                            Log.v("TEST31", "return toucmask obj");

                            return mTouchMask;
                        } else if (tagname.equalsIgnoreCase("comment") && okNumT) {
                            Log.v("TEST31", "add COMMENT: " + text);
                            //extract comment
                            mTouchMask.setComment(text);
                        }
                        break;

                    default:
                        break;
                }
                eventType = parser.next();
            }

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return mTouchMask;
    }
}