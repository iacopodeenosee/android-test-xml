package idnlab.iacopodeenosee.test_xml;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;


public class ActiveProfileActivity extends AppCompatActivity {

    private TextView tvw_activeprofile;
    private TextView tvw_params;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_activeprofile);

        tvw_activeprofile = findViewById(R.id.tvw_activeprofile);
        tvw_params = findViewById(R.id.tvw_params);


        final MatrixData profile = MatrixData.getInstance();
        String Ori;
        if (profile.getOrientation() == 1) {
            Ori = "portrait";
        } else {
            Ori = "landscape";
        }

        String tmpParams = tvw_params.getText() + "\nH: " +
                profile.getHeightScreen() + " | W: " +
                profile.getWidthScreen() + "\nDPI: " +
                profile.getDensityDPI() + " | d: " +
                profile.getDensity() + " | I: " +
                profile.getInches() + "\nO: " +
                Ori + "\nT: " +
                profile.getTypeInput() + "\nN: " +
                profile.getNumberT();

        tvw_params.setText(tmpParams);


        ListView listView = findViewById(R.id.lsw_parseout);

        List<TouchMask> lTM = parseXML();

        ArrayAdapter<TouchMask> adapter = new ArrayAdapter<TouchMask>(this, android.R.layout
                .simple_list_item_1, lTM);
        listView.setAdapter(adapter);

        Button btnGetActiveProfil = findViewById(R.id.btn_getactiveprofile);
        btnGetActiveProfil.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //load active touchmask
                TouchMask ActiveTM = getActiveProfile(profile);
                tvw_activeprofile.setText("ACTIVE PROFILE:\n" + ActiveTM.getComment());
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private List<TouchMask> parseXML() {
        List<TouchMask> listTouchMask = null;

        double tmpI = MatrixData.getInstance().getInches();
        String tmpF = MatrixData.getInstance().getFilePath(tmpI);

        try {
            ParseHandler parser = new ParseHandler();
            InputStream is = getAssets().open(tmpF);
            listTouchMask = parser.parse(is);

            return listTouchMask;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private TouchMask getActiveProfile(MatrixData md) {
        TouchMask tmpTM;

        double tmpI = MatrixData.getInstance().getInches();
        String tmpF = MatrixData.getInstance().getFilePath(tmpI);

        try {
            ParseHandler parser = new ParseHandler();
            InputStream is = getAssets().open(tmpF);
            tmpTM = parser.parseActiveTM(is, md);

            return tmpTM;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
