package idnlab.iacopodeenosee.test_xml;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {

    private String str_XML = "";

    private TextView tvw_statusXML;
    private Button btn_viewxml;
    private Button btn_getactiveprofile;
    private Spinner spn_input;
    private EditText edt_numberT;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        tvw_statusXML = findViewById(R.id.tvw_statusXml);
        spn_input = findViewById(R.id.spn_typeinput);
        edt_numberT = findViewById(R.id.etw_numberT);

        //setup spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.input,
                android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_input.setAdapter(adapter);

        Button btn_getxml = findViewById(R.id.btn_getXML);
        btn_getxml.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //create new file selection based on inches screen
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);
                double tmpI = MatrixData.getInstance().getInches(metrics);
                String tmpF = MatrixData.getInstance().getFilePath(tmpI);

                //start load XML file
                new LoadXmlTask().execute(tmpF);
            }
        });

        btn_viewxml = findViewById(R.id.btn_viewXML);
        btn_viewxml.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                if (str_XML.equals("")) {
                    Toast.makeText(view.getContext(), "FILE XML EMPTY", Toast.LENGTH_LONG);
                } else {
                    String pkg = getPackageName();

                    Intent i = new Intent(getApplicationContext(), XmlActivity.class);

                    i.putExtra(pkg + ".strXML", str_XML);

                    view.getContext().startActivity(i);
                }
            }

        });

        btn_getactiveprofile = findViewById(R.id.btn_getactiveprofile);
        btn_getactiveprofile.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                //get info from device
                String varType = spn_input.getSelectedItem().toString();
                int varNumberT = Integer.parseInt(edt_numberT.getText().toString());
                int orientation = getResources().getConfiguration().orientation;
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);

                MatrixData.getInstance().updateData(varType, orientation,
                        varNumberT, metrics);

                Intent i = new Intent(view.getContext(), ActiveProfileActivity.class);
                view.getContext().startActivity(i);
            }
        });

        btn_viewxml.setEnabled(false);
        btn_getactiveprofile.setEnabled(false);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private String readTextFile(InputStream inputStream) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        byte buffer[] = new byte[1024];
        int size;

        try {
            while ((size = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, size);
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {
            Log.e("TEST_XML", e.getMessage());
            return e.getMessage();
        }
        return outputStream.toString();
    }

    private class LoadXmlTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... path) {
            Log.v("TEST32", "file xml: " + path[0]);
            //load XML from a file
            AssetManager assetManager = getAssets();
            InputStream inputStream = null;
            try {
                inputStream = assetManager.open(path[0]);
            } catch (IOException e) {
                Log.e("TEST_XML", e.getMessage());
                //update textview
                tvw_statusXML.setText("Status XML file:\nFAIL");
                tvw_statusXML.setTextColor(Color.RED);
            }
            return readTextFile(inputStream);
        }

        @Override
        protected void onPostExecute(String result) {
            str_XML = result;

            //update textview
            tvw_statusXML.setText("Status XML file:\n\nREADY");
            tvw_statusXML.setTextColor(Color.GREEN);

            btn_viewxml.setEnabled(true);
            btn_getactiveprofile.setEnabled(true);
        }
    }
}